FROM golang:1.20

# Set the Current Working Directory inside the container
WORKDIR $GOPATH/src/gitlab.com/marceloeduardo244/go-static-server

# Copy everything from the current directory to the PWD (Present Working Directory) inside the container
COPY . .

# Download all the dependencies
RUN go get -d -v ./...

# Install the package
RUN go install -v ./...

# This container exposes port 9000 to the outside world
EXPOSE 9000

# Run the executable
CMD ["go-static-server"]