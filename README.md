![image](https://interestedvideos.com/wp-content/uploads/2023/02/golang-gMW2A.jpg)


# 🚀 Go Static Server

Bem-vindo(a). Este é o Go Static Server!

O objetivo desta aplicação é servir arquivos estáticos, que podem ser de pdfs até builds reactJS ou nextJS.

# 🧠 Contexto

Tecnologias utilizadas neste projeto:
-  Golang
-  Docker
-  Pipeline End to End
    - Na pipeline temos 4 jobs
        - generate_binary para gerar o arquivo binario da app (.bin)
        - deploy_batch_docker para fazer o build e atualizar a imagem no docker-hub

## 📋 Instruções para subir a aplicação utilizando os executaveis

É necessário ter 3 arquivos, dentro de uma mesma pasta.

# Windows
- go-static-server.exe
- .env (configurações)
- static (pasta onde os arquivos ficarão dentro)

# Linux
- go-static-server.bin
- .env (configurações)
- static (pasta onde os arquivos ficarão dentro)

# Arquivo .env
- SERVER_ADDRESS=:9000 (host:porta **se n add o host ele define localhost por padrão)
- MODE
    - MODE=ARCHIVES (Arquivos comuns)
    - MODE=NEXTJS (Build de app NextJS)
    - MODE=REACTJS (Build de app ReactJS)

# Exemplo de config pronta no arquivo .env
SERVER_ADDRESS=:9000
MODE=ARCHIVES

- Por fim execute o .exe no windows e o .bin no linux

## 📋 Instruções para subir a aplicação utilizando o Docker

É necessário ter o Docker instalado e rodando na sua maquina!

# Comandos docker
- docker pull marcelosilva404/go-static-server:1.0 
- docker run --name go-static-server -p 9000:9000 -e SERVER_ADDRESS=:9000 -e MODE=ARCHIVES -v C:/GitEstudo/reactJS/spring-finance-web/build/:/go/src/gitlab.com/marceloeduardo244/go-static-server/static/ -d marcelosilva404/go-static-server:1.0  

# Explicação sobre as variáveis de ambiente (-e)
- SERVER_ADDRESS=:9000 (host:porta **se n add o host ele define localhost por padrão)
- MODE
    - MODE=ARCHIVES (Arquivos comuns)
    - MODE=NEXTJS (Build de app NextJS)
    - MODE=REACTJS (Build de app ReactJS)

# Explicação sobre os volumes (-v)
-  A parte do comando -v C:/GitEstudo/reactJS/spring-finance-web/build/:/go/src/gitlab.com/marceloeduardo244/go-static-server/static/ pega todos os arquivos dentro do diretório build (C:/GitEstudo/reactJS/spring-finance-web/build/) da sua maquina e insere dentro do container go-static-server, mais precisamente no diretório /go/src/gitlab.com/marceloeduardo244/go-static-server/static/

- Por fim ele sobe a aplicação e disponibiliza na porta que vc definiu em -p sua_porta:porta_do_container

Made with 💜 at OliveiraDev
