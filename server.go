package main

import (
	"log"
	"net/http"
	"os"
	"regexp"

	"github.com/joho/godotenv"
)

func main() {
	// Carrega variáveis de ambiente do arquivo .env
	if err := godotenv.Load(); err != nil {
		log.Println("Aviso: Não foi possível carregar o arquivo .env")
	}

	// Lê variáveis de ambiente
	serverAddress := os.Getenv("SERVER_ADDRESS")
	if serverAddress == "" {
		serverAddress = ":8080" // Porta padrão para HTTP
	}

	mode := os.Getenv("MODE")
	if mode == "" {
		mode = "ARCHIVES"
	}

	protocol := os.Getenv("PROTOCOL") // HTTP ou HTTPS
	if protocol == "" {
		protocol = "HTTP" // Padrão para HTTP
	}

	// Configura o servidor de arquivos estáticos
	fileServer := http.FileServer(http.Dir("./static"))

	switch mode {
	case "ARCHIVES", "NEXTJS":
		http.Handle("/", fileServer)
	default:
		fileMatcher := regexp.MustCompile(`\.[a-zA-Z0-9]+$`)
		http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			if !fileMatcher.MatchString(r.URL.Path) {
				http.ServeFile(w, r, "static/index.html")
			} else {
				fileServer.ServeHTTP(w, r)
			}
		})
	}

	if protocol == "HTTPS" {
		// Lê os caminhos dos certificados e chaves privadas
		certFile := os.Getenv("CERT_FILE")
		keyFile := os.Getenv("KEY_FILE")

		if certFile == "" || keyFile == "" {
			log.Fatalf("CERT_FILE e KEY_FILE precisam estar configurados para HTTPS")
		}

		// Mensagem de inicialização
		log.Printf("Servidor HTTPS iniciado no endereço %s no modo: %s\n", serverAddress, mode)

		// Inicia o servidor HTTPS
		if err := http.ListenAndServeTLS(serverAddress, certFile, keyFile, nil); err != nil {
			log.Fatalf("Erro ao iniciar o servidor HTTPS: %v\n", err)
		}
	} else {
		// Mensagem de inicialização
		log.Printf("Servidor HTTP iniciado no endereço %s no modo: %s\n", serverAddress, mode)

		// Inicia o servidor HTTP
		if err := http.ListenAndServe(serverAddress, nil); err != nil {
			log.Fatalf("Erro ao iniciar o servidor HTTP: %v\n", err)
		}
	}
}
